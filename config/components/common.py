from config.settings import env, root


# Build paths inside the project like this: root(...)
BASE_DIR = root()


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env('SECRET_KEY')

ALLOWED_HOSTS = env.list('ALLOWED_HOSTS', default=[])

ROOT_URLCONF = 'config.urls'

WSGI_APPLICATION = 'config.wsgi.application'

CORS_ORIGIN_WHITELIST = env.list('CORS_ORIGIN_WHITELIST', default=[])

SHOW_DOCS = env('SHOW_DOCS', default=False)

FIXTURE_DIRS = [
    root('db', 'fixtures'),
]
