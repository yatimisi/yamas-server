# Yamas Server

> This is yamas website API version 0.0.1

## Installation

1. Set up your SSH keys to connect to Github.
1. In your terminal, change to the directory that you want to store the files.
1. `git clone git@github.com:yatimisi/yamas-server.git`: Using SSH protocal to clone the repo.
1. `pipenv install` or `pipenv install --dev`: Create a virtual environment.
1. `pipenv shell`: Activate the virtual environment.
1. `cd yamas-server`
1. `python manage.py migrate`: Please migrate the database whenever the DB structure were updated.

## Running

Run the command `python manage.py runserver 127.0.0.1:8000` in your Yamas working directory to start the server.

## Notices
1. Whenever the `model.py` were changed, please run the commands `python manage.py makemigrations` and `python manage.py migrate` to migrate your local DB structure.
